=== LockDownWP ===
Contributors: mgkimsal
Donate link: https://example.com/
Tags: security, files
Requires at least: 5.0
Tested up to: 5.0.2
Stable tag: 1.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Keep all your files locked down - prevent web shells, site hijacking and many common exploits.

== Description ==

LockDown WP provides a quick way to lock down *all* your files - plugins, themes, WordPress core and
anything else on your site.

One of the most common WordPress exploits is malware modifying your existing files, with the
goal of

*   adding hidden redirects to spam pages
*   adding SEO links for other sites
*   adding web shells to send spam emails
*   and more...

Note that *ALL* files are locked down - made unwritable - which means that even auto-updates to
plugings, themes and WP core will not happen until you explicitly 'unlock' your system.


== Frequently Asked Questions ==

= My WordPress won't update! =

Once LockDownWP is activated, all files under your WordPress installation are unwritable, including
core system files.  Please unlock your site for a few minutes, then perform any updates that need
to be done.

= How do I disable this plugin? =

You can deactivate the plugin, but you should click the 'restore permissions' button first,
to ensure that your files permissions are restored to their original state first.

== Screenshots ==

1. Lock down screen
2. Admin panel notices when unlocked

== Changelog ==

= 1.1.0 =
* Whitelist file/path functionality

= 1.0.7 =
* Documentation update (this changelog)

= 1.0.6 =
* Keep original file permissions and allow 'restore' of all original file permissions

= 1.0.5 =
* Testing out auto-updater

