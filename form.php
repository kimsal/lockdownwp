<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></link>
<?php
// shouldn't there be a better way to do this in wordpress?
$offset = get_option('gmt_offset') * 60 * 60;
?>
<style>
    .button {
        width: 100%;
    }

    .button-open {
        background-color: red !important;
        color: white !important;
    }

    .button-is-open {
        background-color: #38d83e !important;
        color: white !important;
    }

</style>
<div class="wrap">
    <div>LockDownWP Settings (v<?= $this->version; ?>)</div>
    <div>Current status: <?= $this->openStatus; ?>
        <?php if ($this->openUntil != null) { ?>
            until
            <?= date(get_option("date_format"), $this->openUntil + $offset); ?>
            <?= date(get_option("time_format"), $this->openUntil + $offset); ?>
            (current time is <?= date(get_option("date_format"), time() + $offset); ?> <?= date(get_option("time_format"), time() + $offset); ?>)
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-md-4">

            <form method="POST">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>
                            <button name="button" class="button button-hero <?= $this->openStatus != 'closed' ? 'button-is-open' : ''; ?>"
                                    value="<?= LDWPPlugin::STATUS_CLOSED; ?>">LOCK DOWN
                                NOW
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button name="button" class="button button-open button-controls button-hero" value="60">UNLOCK ALL FOR 1 MINUTE</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button name="button" class="button button-open sbutton-controls button-hero" value="300">UNLOCK ALL FOR 5 MINUTES</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button name="button" class="button button-open button-controls button-hero" value="900">UNLOCK ALL FOR 15 MINUTES</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button name="button" class="button button-open button-controls button-hero" value="1800">UNLOCK ALL FOR 30 MINUTES</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button name="button" class="button button-open button-controls button-hero" value="3600">UNLOCK ALL FOR 60 MINUTES</button>
                        </td>
                    </tr>
                    </tbody>
                </table>


                <br/>
            </form>
            <form method="post">
                <button name="restore"  class="btn btn-success btn-block">Restore original permissions</button>
            </form>
        </div>
        <div class="col-md-8">
            <h4>Whitelist folders</h4>
            <form method="post">
                <textarea class="form-control" name="folders"><?php foreach($this->whitelistFolders as $w) { echo str_replace($this->homePath,"",$w)."\n"; } ?></textarea>
                <br/>
                <button name="whitelist"  class="btn btn-success btn-block">Ignore these folders and contents</button>
            </form>


            <hr/>
            <?php if ($this->apiKey == null) { ?>
            <div class="">

                <h4>A pro version is in development!</h4>
                <h4>Cool features are in the works like...</h4>
                <ul>
                    <li>Unlock/log log reports</li>
                    <li>SMS/email alerts on unlock</li>
                    <li>SMS verification code to unlock</li>
                </ul>
                <p>
                    Got an idea you'd like to see in the next version?  Email support@lockdownwp.com with your ideas and feedback!
                </p>
            </div>
            <div class="card">
                <h5>BETA - Have an API key?</h5>
                <form method="post">
                    <input type="text" class="form-control" name="apikey"/>
                    <button class="btn btn-success">Add my API key</button>
                </form>
            </div>
            <?php } ?>
        </div>
    </div>

</div>
