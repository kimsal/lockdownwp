<?php

/**
 * Plugin Name:     LockDownWP
 * Plugin URI:      https://lockdownwp.com
 * Description:     Lock down your WP file system
 * Author:          Michael Kimsal
 * Author URI:      kims.al
 * Text Domain:     lockdownwp
 * S
 * Version:         1.1.0
 *
 * @package         LockDownWP
 */


$ldwpPlugin = new LDWPPlugin();


add_action('plugins_loaded', [$ldwpPlugin, 'settings']);
add_filter('wp_headers', [$ldwpPlugin, 'nocache']);

require 'plugin-update-checker-4.4/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	LDWPPlugin::BB_REPO,
	__FILE__,
	$ldwpPlugin->slug
);

class LDWPPlugin
{

    const BB_REPO = 'https://bitbucket.org/kimsal/lockdownwp';
    public $originalPerms = [];
    public $status = null;
    public $openUntil = null;
    public $openMinutes = 5;
    public $openStatus = null;
    public $version = "1.1.0";
    public $versionChecked = null;
    public $whitelistFolders = [];
    public $page = "lockdownwp";
    public $slug = "lockdownwp";  // slug and page need to be same
    public $transientSlug = "ldwp_transient_lockdownwp";
    public $apiKey = null;
    public $apiKeyRefreshed = null;
//    private $apiServer = "https://lockdownwp.com/";

    private $apiServer = "https://lockdownwp.com/api";

    public $homePath = '';

    public $debugLog = false;
    public $logFile = "/tmp/ldwp_log.txt";

    const STATUS_NULL    = null;
    const STATUS_UNKNOWN = "unknown";
    const STATUS_OPEN    = "unlocked";
    const STATUS_CLOSED  = "locked";

    public function __construct()
    {
        require_once(ABSPATH . 'wp-admin/includes/file.php');

        $this->homePath = get_home_path();
    }

    public function init()
    {
        ob_start();
        add_action('admin_menu', [$this, 'page']);
        add_action('hook_close_perm', [$this, 'close_perm']);
        add_action('wp_before_admin_bar_render', [$this, 'admin_header']);

        $this->getParams();

    }

    public function getParams()
    {
        $this->openUntil       = get_option("ldwp_open_until", null);
        $this->openStatus      = get_option("ldwp_open_status", self::STATUS_UNKNOWN);
        $this->openMinutes     = get_option("ldwp_open_status", 5);
        $this->apiKey          = get_option("ldwp_api_key", null);
        $this->apiKeyRefreshed = get_option("ldwp_api_key_refreshed", null);
        $this->version         = get_option("ldwp_version", $this->version);
        $this->originalPerms   = get_option("ldwp_original_perms");
        $this->whitelistFolders   = get_option("ldwp_whitelist_folders", '');
        $this->whitelistFolders = explode("\n", trim($this->whitelistFolders));

        foreach($this->whitelistFolders as &$x)
        {
		$x = trim($x);
		if($x=='' ) { continue; }
            $x = $this->homePath.$x;
            if(substr($x,-1)!="/") {
//                $x.= "/";
            }
        }
    }


    public function checkApiKey()
    {
        if ($this->apiKey != null) {
            if ($this->apiKeyRefreshed == null || $this->apiKeyRefreshed < time() - 7200) {
                $x = file_get_contents($this->apiServer . "apicheck/" . $this->apiKey);
                if ($x) {
                    $this->apiKeyRefreshed = time();
                } else {
                    $this->apiKeyRefreshed = null;
                }
                update_option("ldwp_api_key_refreshed", $this->apiKeyRefreshed);
            }
        }
    }


    public function settings()
    {
        $this->init();
    }

    public function admin_header()
    {
        $this->getParams();

        if ($this->openStatus != self::STATUS_CLOSED) {
            if ($this->openStatus == self::STATUS_OPEN && $this->openUntil!=null) {
                $time    = $this->getDisplayCloseTime();
                $warning = <<<EOD
<div class="notice notice-error">
    <p>NOTICE - your files are unlocked until $time.  <a href="admin.php?page=$this->page">Click here to lock down.</a></p>
</div>
EOD;
            } else {
                $warning = <<<EOD
<div class="notice notice-error">
    <p>NOTICE - your files may be unlocked.  <a href="admin.php?page=$this->page">Click here to lock down.</a></p>
</div>
EOD;

            }
            echo $warning;
        }

    }

    public function page()
    {
        $page_title = 'LockDownWP';
        $menu_title = 'LockDownWP';
        $capability = 'manage_options';
        $menu_slug  = $this->page;
        $function   = [$this, 'form'];
        $icon_url   = 'dashicons-lock';
        $position   = 24;

        add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position);
    }

    public function close_perm()
    {
        $this->getParams();
        $this->chmodr(get_home_path(), 0555);
        update_option("ldwp_open_status", self::STATUS_CLOSED);
        update_option("ldwp_open_until", null);
    }

    public function getDisplayCloseTime()
    {
        $offset = get_option('gmt_offset') * 60 * 60;

        if($this->openUntil!=null) {
            $x = date(get_option("date_format"), $this->openUntil + $offset) . " ";
            $x .= date(get_option("time_format"), $this->openUntil + $offset);
        } else {
            $x = "";
        }

        return $x;
    }

    public function getTargetCloseTimestamp($target = 60)
    {
        if ($target > 7200) {
            $target = 7200;
        }
        if ($target < 0) {
            $target = 0;
        }
        $seconds = time() + $target;

        return $seconds;
    }

    public function form()
    {

        if(isset($_POST['whitelist']))
        {
            $d = trim($_POST['folders']);
            update_option("ldwp_whitelist_folders", $d);
            $this->whitelistFolders = explode("\n", $d);
            wp_redirect(admin_url("admin.php?page=".$this->page));
            die();
        }

        if($this->originalPerms==null)
        {
            $this->getOriginalPerms(get_home_path());
        }
        if(isset($_POST['restore']))
        {
            $this->restoreOriginalPerms();
            wp_redirect(admin_url("admin.php?page=".$this->page));
            die();
        }
        if (isset($_POST['button'])) {
            $mode = 0555;
            if ($_POST['button'] != self::STATUS_CLOSED) {
                $mode  = 0777;
                $until = $this->getTargetCloseTimestamp((int)$_POST['button']);
                update_option("ldwp_open_status", self::STATUS_OPEN);
                update_option("ldwp_open_until", $until);
                wp_schedule_single_event($until, 'hook_close_perm');

            } else {
                update_option("ldwp_open_status", self::STATUS_CLOSED);
                update_option("ldwp_open_until", null);
            }
            $this->chmodr(get_home_path(), $mode);
            ob_clean();
            wp_redirect(admin_url("admin.php?page=".$this->page));
            die();

        }
        $this->getParams();
        include("form.php");
        ob_end_flush();
    }

    function chmodr($path, $mode = 0555)
    {
        $this->log("hitting $path");
        if(in_array($path, $this->whitelistFolders))
        {
            $this->log("$path was in whitelistFolders");
            return;
        }
        $this->log("$path not whitelisted...");
        $dir = new DirectoryIterator($path);
        foreach ($dir as $item) {

            if(in_array($item->getPathname(), $this->whitelistFolders))
            {
                $this->log("$path was in whitelistFolders");
                return;
            }
//            $this->log("chmod $path ".decoct($mode));
            if(!$item->isDot()) {
                chmod($item->getPathname(), ($mode));
            } else {

                $this->log($item->getPathname()." is dot - skipping chmod");

            }
//            $this->log("chmod $path ".decoct($mode));
            if ($item->isDir() && !$item->isDot()) {
//                $this->log("chmod r call to ".$item->getPathname());
                $this->chmodr($item->getPathname(), $mode);
            }
        }
        return;
    }

    public function getOriginalPerms($path)
    {
        if($this->originalPerms==null){ $this->originalPerms = []; }
        $this->_getOriginalPerms($path);
        update_option("ldwp_original_perms", $this->originalPerms);
    }

    function _getOriginalPerms($path)
       {
           $dir = new DirectoryIterator($path);
           foreach ($dir as $item) {
               if(!array_key_exists($item->getPathname(), $this->originalPerms)) {
                   $this->originalPerms[$item->getPathname()] = $item->getPerms();
               }
               if ($item->isDir() && !$item->isDot()) {
                   $this->_getOriginalPerms($item->getPathname());
               }
           }
       }

    public function restoreOriginalPerms()
    {
        foreach($this->originalPerms as $file=>$perm)
        {
            if(fileperms($file)!=$perm)
            {
                chmod($file, $perm);
            }
        }
        update_option("ldwp_open_status", self::STATUS_OPEN);
        update_option("ldwp_open_until", null);
    }

    public function nocache($headers)
    {
        unset($headers['Cache-Control']);
        return $headers;
    }

    public function log($info)
    {
        if($this->debugLog) {
            file_put_contents($this->logFile, $info . "\n", FILE_APPEND);
        }
    }

    public function clearLog()
    {
        unlink($this->logFile);
    }
}
